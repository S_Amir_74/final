package me.test.afinal;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class Fragment_builder extends FragmentPagerAdapter {
    private final ArrayList<Fragment> pages = new ArrayList<>();
    private final ArrayList<String>pages_title = new ArrayList<>();

    public Fragment_builder(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return pages.get(i);
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    public void add_fragment(Fragment fragment , String title){
        pages.add(fragment);
        pages_title.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
    super.getPageTitle(position);
    return pages_title.get(position);
    }
}

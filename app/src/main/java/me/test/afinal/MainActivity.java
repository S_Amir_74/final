package me.test.afinal;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import me.test.afinal.papers.Daily;
import me.test.afinal.papers.Monthly;
import me.test.afinal.papers.Totally;
import me.test.afinal.papers.Weekly;
import me.test.afinal.papers.Yearly;

public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager)findViewById(R.id.viewpaper);
        tabLayout = (TabLayout)findViewById(R.id.tabs);
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        tabLayout.setupWithViewPager(viewPager);
        setuppaper(viewPager);

    }

    public void setuppaper(ViewPager viewPager){
        Fragment_builder builder = new Fragment_builder(getSupportFragmentManager());
        builder.add_fragment(new Totally() , "Totally");
        builder.add_fragment(new Daily() , "Daily");
        builder.add_fragment(new Weekly() , "Weekly");
        builder.add_fragment(new Monthly() , "Monthly");
        builder.add_fragment(new Yearly() , "Yearly");

        viewPager.setAdapter(builder);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu , menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.add_new){

        }
        return super.onOptionsItemSelected(item);
    }
}

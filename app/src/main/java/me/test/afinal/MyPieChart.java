package me.test.afinal;

import android.graphics.Color;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class MyPieChart {

    private ArrayList<Entry> yvalues = new ArrayList<Entry>();
    private ArrayList<String> xVals = new ArrayList<String>();

    public void add_YX_values(ArrayList<Integer> values , ArrayList<String> xVales){

        for(int i = 0 ; i< values.size();i++){
            yvalues.add(new Entry(values.get(i), i));
            xVals.add(xVales.get(i));
        }


    }


    public PieData pieData(){
        if(xVals != null) {
            PieData data = new PieData(xVals, pieDataSet());
            data.setValueFormatter(new PercentFormatter());
            data.setValueTextColor(Color.DKGRAY);
            return data;
        }else
            return null;
    }

    public PieDataSet pieDataSet(){
        if(yvalues != null) {
            PieDataSet dataSet = new PieDataSet(yvalues, "Election Results");
            dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
            return dataSet;
        }else
            return null;
    }


}



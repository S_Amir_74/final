package me.test.afinal.papers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

import me.test.afinal.MyPieChart;
import me.test.afinal.R;

public class Totally extends Fragment implements OnChartValueSelectedListener {
    private PieChart pieChart;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.totally , container , false);
        pieChart = view.findViewById(R.id.piechart);
        MyPieChart myPieChart = new MyPieChart();
        pieChart.setUsePercentValues(true);


        ArrayList<Integer> mydata = new ArrayList<>();
        ArrayList<String> mydata_name = new ArrayList<>();
        mydata.add(20);
        mydata.add(30);
        mydata.add(10);
        mydata_name.add("Buy");
        mydata_name.add("Sell");
        mydata_name.add("spend");

        myPieChart.add_YX_values(mydata , mydata_name);
        pieChart.setData(myPieChart.pieData());
        pieChart.setDescription("This is Pie Chart");
        myPieChart.pieDataSet();
        pieChart.setOnChartValueSelectedListener(this);


        super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
